import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  title: string = "abc";
  shapes: any= [
    {name: 'Rectangle', value: '1' },
    {name: 'Circle', value: '2' },
    {name: 'Square', value: '3' },
    {name: 'Ellipse', value: '4' }
    ];
    selectedVal: number;
    selectedShape: string;
    pageOne: boolean;
    pageTwo: boolean;
    pageThree: boolean;
    area: any;
    rectLength: any;
    rectWidth: any;
    diameter: any;
    side: any;
    ellipLength: any;
    ellipWidth: any;
    selectShapeText: string;
    shapeRequired: boolean;
    currentPage: number;
    inputValidation: boolean;
  constructor() {
  }

  ngOnInit() {
this.pageOne = true;
this.pageThree = false;
this.pageTwo = false;
this.shapeRequired = false;
this.currentPage = 0;
  }

goToPage(page: number){
    switch ( page) {
      case 1:
      this.pageOne = true;
      this.pageTwo = false;
      this.pageThree = false;
      this.resetPage();
      break;
      case 2:
      this.pageOne = false;
      this.pageTwo = true;
      this.pageThree = false;
      this.shapeRequired = false;
      break;
      default:
      this.shapeRequired = true;
    }
  }

  checkedShape(id: number) {
    this.selectedVal = id;
  this.currentPage = 2;
    let selectedShape;
    this.shapes.map(function(currValue, index){
      if(currValue.value === id) {
        selectedShape = currValue.name;
      }
    });
    this.selectedShape = selectedShape;
  }

  calculateArea(shapeId: string) {
    switch (shapeId) {
case '1':
if( (this.rectLength !== null && this.rectLength !== undefined) && (this.rectWidth !== null || this.rectWidth !== undefined)) {
  this.pageThree = true;
  this.pageOne = false;
  this.pageTwo = false;
this.area = this.rectLength * this.rectWidth;
this.selectShapeText = 'Length:' + this.rectLength + ' and Width:' + this.rectWidth;
}else {
this.inputValidation = true;
}
break;
case '2':
if (this.diameter !== null && this.diameter !== undefined) {
  this.pageThree = true;
  this.pageOne = false;
  this.pageTwo = false;
this.area = Math.round(Math.PI * (this.diameter / 2) * (this.diameter / 2));
this.selectShapeText = 'Diameter' + this.diameter;
}else {
  this.inputValidation = true;
}
break;
case '3':
if (this.side !== null && this.side !== undefined) {
this.pageThree = true;
this.pageOne = false;
this.pageTwo = false;
this.area = this.side * this.side;
this.selectShapeText = 'Width:' + this.side;
}else {
this.inputValidation = true;
}
break;
case '4':
if ((this.ellipLength !== null && this.ellipLength !== undefined) && (this.ellipWidth !== null && this.ellipWidth !== undefined)) {
this.pageThree = true;
this.pageOne = false;
this.pageTwo = false;
this.area = Math.round(Math.PI * (this.ellipLength / 2) * (this.ellipWidth / 2));
this.selectShapeText = 'Length' + this.ellipLength + ' width' + this.ellipWidth;
} else {
  this.inputValidation = true;
}
break;
    }
  }

  resetPage() {
    this.selectedVal = null;
    this.selectedShape = null;
    this.pageOne = true;
    this.pageTwo = false;
    this.pageThree = false;
    this.area = null;
    this.rectLength = null;
    this.rectWidth = null;
    this.diameter = null;
    this.side = null;
    this.ellipLength = null;
    this.ellipWidth = null;
    this.currentPage = 0;
    this.inputValidation = false;
  }
}
