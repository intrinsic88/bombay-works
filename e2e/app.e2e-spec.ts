import { BombayWorksPage } from './app.po';

describe('bombay-works App', function() {
  let page: BombayWorksPage;

  beforeEach(() => {
    page = new BombayWorksPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
